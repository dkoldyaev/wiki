__author__ = 'DKOLDYAEV'

from django.forms import ModelForm

from page.models import Page

class PageForm(ModelForm):
    class Meta:
        model = Page
        fields = ['name', 'slug', 'text']