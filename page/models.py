# -*- coding: utf-8 -*-
__author__ = 'DKOLDYAEV'

from django.db import models

class PageAbstract(models.Model):

    _active = models.BooleanField(blank=False, default=True)
    _comment = models.TextField(blank=True)
    _created = models.DateTimeField(auto_now_add=True)
    _updated = models.DateTimeField(auto_now=True)

    class Meta:
        abstract = True


class Page(PageAbstract):

    name = models.CharField(max_length=255, blank=False)
    slug = models.SlugField(blank=True, null=False)
    text = models.TextField(blank=True)

    path = models.CharField(max_length=255, blank=True, null=False, unique=True, db_index=True)
    parent = models.ForeignKey('page.Page', related_name='childs', blank=True, null=True)

    def clean(self):

        errors = {}

        errors_slug = []
        self._generate_slug()
        uncorrect_slugs = ['add', 'edit', 'delete']
        if self.slug in uncorrect_slugs:
            errors_slug.append(u'В поле slug запрещены следующие значения: «' + u'», «'.join(uncorrect_slugs) + u'»')

        try:    self.path = self.parent.path + '/' + self.slug
        except: self.path = self.slug
        if Page.objects.filter(path = self.path).exclude(id=self.id).count() > 0 :
            errors_slug.append(u'Страница с адресом /%s уже существует' % self.path)

        if len(errors_slug):
            errors['slug'] = errors_slug

        if len(errors) > 0:
            from django.core.validators import ValidationError
            raise ValidationError(errors)

    def _generate_slug(self):

        from pytils.translit import slugify
        if self.slug == '':
            self.slug  = slugify(self.name).replace('-', '_')

    def __unicode__(self):

        return u' — ' * len(self.path.split('/')) + self.name

    class Meta:
        ordering = ['path', 'id']