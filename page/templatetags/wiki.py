__author__ = 'DKOLDYAEV'

from django import template
from django.template.defaultfilters import stringfilter

register = template.Library()

@register.filter(name='wiki')
@stringfilter
def wiki(text):

    import re

    re_bold = re.compile(r'\*\*(.+?)\*\*', re.DOTALL)
    re_italic = re.compile(r'//(.*)//', re.DOTALL)
    re_underline = re.compile(r'__(.*)__', re.DOTALL)

    text = re_bold.sub(u'<strong>\\1</strong>', text)
    text = re_italic.sub(u'<em>\\1</em>', text)
    text = re_underline.sub(u'<u>\\1</u>', text)

    re_local_link =     re.compile(r'\[\[([a-z0-9_/]+)(\s+.+?)?\]\]', re.DOTALL)
    from django.core.urlresolvers import reverse
    from page.views import _get_page_by_path
    for link, link_text in re_local_link.findall(text):
        style = ''
        link_text = link_text.strip()
        if link_text == '':
            link_text = link
        if _get_page_by_path(link) is None:
            link = reverse('page.views.add', kwargs={'path': link})
            style = 'color:red;'
        else:
            link = reverse('page.views.show', kwargs={'path': link})
        text = re_local_link.sub(
            u'<a href="%s" style="%s">%s</a>' % (link, style, link_text),
            text,
            1
        )

    #TODO: add GET-parameters
    re_external_link = re.compile(r'\[\[((https?://)?[\sa-z\.-_]+\.[a-z\.]{2,6}/?[^\s]*)(\s+.+)?\]\]', re.DOTALL)
    for link, protocol, link_text in re_external_link.findall(text):
        link_text = link_text.strip()
        if link_text == '':
            link_text = link
        if protocol == '':
            link = 'http://' + link
        text = re_external_link.sub(
            u'<a href="%s" target="_blank">%s</a>' % (link, link_text),
            text,
            1
        )

    return text
