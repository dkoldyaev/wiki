__author__ = 'DKOLDYAEV'

def _get_page_by_path(path):

    from page.models import Page
    try:
        return Page.objects.get(path=path)
    except:
        return None

def show(request, path):

    page = _get_page_by_path(path)
    if page is None:
        from django.shortcuts import Http404
        raise Http404

    from django.shortcuts import render

    return render(
        request,
        'page/show.html',
        {
            'page':page
        }
    )

def add_root(request):
    return add(request, path=None)

def add(request, path=None):

    from page.forms import PageForm
    parent_page = _get_page_by_path(path)

    if parent_page is None and path is not None:
        from django.shortcuts import Http404
        raise Http404

    if request.method == 'POST':
        from page.models import Page
        new_page = Page(parent=parent_page)
        form = PageForm(request.POST, instance=new_page)
        if form.is_valid():
            success = form.save()
            from django.shortcuts import redirect
            return redirect('page.views.show', path=success.path)
    else:
        form = PageForm()

    from django.shortcuts import render

    return render(
        request,
        'page/add.html',
        {
            'form':         form,
            'parent_page':  parent_page
        }
    )

def edit(request, path):

    from page.forms import PageForm
    edited_page = _get_page_by_path(path)

    if edited_page is None:
        from django.shortcuts import Http404
        raise Http404

    if request.method == 'POST':
        form = PageForm(request.POST, instance=edited_page)
        if form.is_valid():
            success = form.save()
            from django.shortcuts import redirect
            return redirect('page.views.show', path=success.path)
    else:
        form = PageForm(instance=edited_page)

    from django.shortcuts import render

    return render(
        request,
        'page/edit.html',
        {

            'form':         form,
            'edited_page':  edited_page
        }
    )

def delete(request, path):

    page = _get_page_by_path(path)
    if path is None:
        from django.shortcuts import Http404
        raise Http404

    if request.method == 'POST' and request.POST.get('YES', '') != '':
        parent_page = page.parent
        page.delete()

        from django.shortcuts import redirect
        if parent_page is None:
            return redirect('page_add_root')
        else:
            return redirect('page.views.show', path=parent_page.path)

    elif request.method == 'POST' and request.POST.get('NO', '') != '':
        from django.shortcuts import redirect
        return redirect('page.views.show', path=path)

    else:
        from django.shortcuts import render
        return render(
            request,
            'page/delete.html',
            {'page':page}
        )

