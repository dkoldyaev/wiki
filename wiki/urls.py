from django.conf.urls import patterns, include, url

# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'wiki.views.home', name='home'),
    # url(r'^wiki/', include('wiki.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    url(r'^admin/', include(admin.site.urls)),
)

from wiki.settings import DEBUG
if DEBUG:
    import debug_toolbar
    urlpatterns += patterns('',
        url(r'^__debug__/', include(debug_toolbar.urls)),
    )

urlpatterns += patterns(
    '',
    url(r'^$', 'wiki.views.homepage', name='homepage'),

    url(r'^add$', 'page.views.add_root', name='add_root'),

    url(r'^(?P<path>[a-z0-9_/]+)/add', 'page.views.add', name='page_add'),
    url(r'^(?P<path>[a-z0-9_/]+)/edit', 'page.views.edit', name='page_edit'),
    url(r'^(?P<path>[a-z0-9_/]+)/delete', 'page.views.delete', name='page_delete'),
    url(r'^(?P<path>[a-z0-9_/]+)$', 'page.views.show', name='page_show')

)
